/*******************************************************************************************
  SIMPL+ Module Information
  (Fill in comments below)
*******************************************************************************************/
/*
Dealer Name:
System Name:
System Number:
Programmer: 
Comments:

Copyright � 2017 Norgate AV Holdings Ltd

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

/*******************************************************************************************
  Compiler Directives
  (Uncomment and declare compiler directives as needed)
*******************************************************************************************/
// #ENABLE_DYNAMIC
#SYMBOL_NAME "NVX Router"
// #HINT ""
#DEFINE_CONSTANT	MAX_IO	32
#CATEGORY "46" "Crestron" 
// #PRINT_TO_TRACE
// #DIGITAL_EXPAND 
// #ANALOG_SERIAL_EXPAND 
// #OUTPUT_SHIFT 
// #HELP_PDF_FILE ""
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_TRACE
// #ENCODING_ASCII
// #ENCODING_UTF16
// #ENCODING_INHERIT_FROM_PARENT
// #ENCODING_INHERIT_FROM_PROGRAM
/*
#HELP_BEGIN
   (add additional lines of help lines)
#HELP_END
*/

/*******************************************************************************************
  Include Libraries
  (Uncomment and include additional libraries as needed)
*******************************************************************************************/
// #CRESTRON_LIBRARY ""
#USER_LIBRARY "NAVFoundation"

/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
  (Uncomment and declare inputs and outputs as needed)
*******************************************************************************************/
// DIGITAL_INPUT
ANALOG_INPUT	Encoder; 
STRING_INPUT	_skip_, Decoder_Stream_Location_Fb[NAV_MAX_CHARS],
				_skip_, Encoder_Stream_Location_Fb[MAX_IO, MAX_IO][NAV_MAX_CHARS]; 
				
// BUFFER_INPUT 

// DIGITAL_OUTPUT 
ANALOG_OUTPUT	Encoder_Fb;
STRING_OUTPUT	_skip_, Decoder_Stream_Location; 

/*******************************************************************************************
  SOCKETS
  (Uncomment and define socket definitions as needed)
*******************************************************************************************/
// TCP_CLIENT
// TCP_SERVER
// UDP_SOCKET

/*******************************************************************************************
  Parameters
  (Uncomment and declare parameters as needed)
*******************************************************************************************/
// INTEGER_PARAMETER
// SIGNED_INTEGER_PARAMETER
// LONG_INTEGER_PARAMETER
// SIGNED_LONG_INTEGER_PARAMETER
// STRING_PARAMETER

/*******************************************************************************************
  Parameter Properties
  (Uncomment and declare parameter properties as needed)
*******************************************************************************************/
/*
#BEGIN_PARAMETER_PROPERTIES parameter_variable, parameter_variable, ...
   // propValidUnits = // unitString or unitDecimal|unitHex|unitPercent|unitCharacter|unitTime|unitTicks;
   // propDefaultUnit = // unitString, unitDecimal, unitHex, unitPercent, unitCharacter, unitTime or unitTicks;
   // propBounds = lower_bound , upper_bound;
   // propDefaultValue = ;  // or, propDefaultValue = "";
   // propList = // { "value" , "label" } , { "value" , "label" } , ... ;
   // propShortDescription = "status_bar_hint_text";
   // #BEGIN_PROP_FULL_DESCRIPTION  line_1...  line_2...  line_n  #END_PROP_FULL_DESCRIPTION
   // #BEGIN_PROP_NOTES line_1...  line_2...  line_n  #END_PROP_NOTES
#END_PARAMETER_PROPERTIES
*/

/*******************************************************************************************
  Structure Definitions
  (Uncomment and define structure definitions as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: struct.myString = "";
*******************************************************************************************/
/*
STRUCTURE MyStruct1
{
};

MyStruct1 struct;
*/

/*******************************************************************************************
  Global Variables
  (Uncomment and declare global variables as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: myString = "";
*******************************************************************************************/
// INTEGER 	
// LONG_INTEGER
// SIGNED_INTEGER
// SIGNED_LONG_INTEGER
// STRING

/*******************************************************************************************
  Functions
  (Add any additional functions here)
  Note:  Functions must be physically placed before the location in
         the code that calls them.
*******************************************************************************************/
function Reset() {
	
}

function Route(integer iParam) {
 	if(iParam && len(Encoder_Stream_Location_Fb[iParam])) {
    	Decoder_Stream_Location = Encoder_Stream_Location_Fb[iParam];
	} else {
    	Decoder_Stream_Location = "";
	}
}

integer_function FindStreamInArray(string sParam) {
	integer x;
	for(x = 1 to MAX_IO) {
		if(Encoder_Stream_Location_Fb[x] = sParam) {
			return(x);
		}
	}
	
	return(0);
}

function Feedback() {
	Encoder_Fb = FindStreamInArray(Decoder_Stream_Location_Fb);
}

/*******************************************************************************************
  Event Handlers
  (Uncomment and declare additional event handlers as needed)
*******************************************************************************************/
change Encoder {
	Route(Encoder);
}

change Encoder_Stream_Location_Fb {
	integer iEncoder;
	iEncoder = getlastmodifiedarrayindex();
	if(iEncoder = Encoder) {
		Route(iEncoder);
	}
	
	Feedback();
}

change Decoder_Stream_Location_Fb {
	Feedback();
}

/*******************************************************************************************
  Main()
  Uncomment and place one-time startup code here
  (This code will get called when the system starts up)
*******************************************************************************************/
Function Main(){
	if(waitforinitializationcomplete() = 0) {
		Reset();
	}
}
